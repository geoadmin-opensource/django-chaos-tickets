# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.conf.urls import url, include

from chaos_tickets.urls import urlpatterns as chaos_tickets_urls

urlpatterns = [
    url(r'^', include(chaos_tickets_urls, namespace='chaos_tickets')),
]
