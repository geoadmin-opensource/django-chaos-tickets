=====
Usage
=====

To use Django Chaos Tickets in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'chaos_tickets.apps.ChaosTicketsConfig',
        ...
    )

Add Django Chaos Tickets's URL patterns:

.. code-block:: python

    from chaos_tickets import urls as chaos_tickets_urls


    urlpatterns = [
        ...
        url(r'^', include(chaos_tickets_urls)),
        ...
    ]
