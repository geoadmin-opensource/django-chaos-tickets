============
Installation
============

At the command line::

    $ easy_install django-chaos-tickets

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-chaos-tickets
    $ pip install django-chaos-tickets
