# -*- coding: utf-8
from django.apps import AppConfig


class ChaosTicketsConfig(AppConfig):
    name = 'chaos_tickets'
