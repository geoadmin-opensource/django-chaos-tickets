.. :changelog:

History
-------

0.1.0 (2017-10-27)
++++++++++++++++++

* First release on PyPI.
